import React from "react";
import { Container } from 'reactstrap';
import HeadingRound from "./heading-round";
import Divider from "../common/divider";
import FeatureRow from "../feature/feature-row";

const Page = () => (
  <Container>
    <HeadingRound />
    <Divider/>
    <FeatureRow placement="left"/>
    <Divider/>
    <FeatureRow placement="right"/>
    <Divider/>
    <FeatureRow placement="left"/>
    <Divider/>
  </Container>
);

export default Page;
