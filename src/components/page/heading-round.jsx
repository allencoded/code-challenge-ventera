import React from "react";
import { Row, Col } from 'reactstrap';
import RoundCard from "../cards/round-card";

const HeadingRound = () => (
  <Row>
    <Col lg="4">
      <RoundCard/>
    </Col>
    <Col lg="4">
      <RoundCard/>
    </Col>
    <Col lg="4">
      <RoundCard/>
    </Col>
  </Row>
);

export default HeadingRound;
