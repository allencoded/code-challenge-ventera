import React from "react";
import { Row, Col } from "reactstrap";
import PlaceholderImage from "../common/placeholder-image";
import "./feature-row.scss";

const FeatureRow = ({ placement }) => (
  <Row>
    {placement === "left" && (
      <>
        <Col lg="7" className="col-center">
          <h2 className="feature-heading">
            First featurette heading.{" "}
            <span className="text-muted">It'll blow your mind.</span>
          </h2>
          <p className="content">
            Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id
            ligula porta felis euismod semper. Praesent commodo cursus magna,
            vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus
            commodo.
          </p>
        </Col>
        <Col lg="5">
          <PlaceholderImage height="500" width="500" />
        </Col>
      </>
    )}
    {placement === "right" && (
      <>
        <Col lg={{ size: 5, order: 2 }} className="col-center">
          <h2 className="feature-heading">
            Oh yeah, it's that good.{" "}
            <span className="text-muted">See for yourself.</span>
          </h2>
          <p className="content">
            Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id
            ligula porta felis euismod semper. Praesent commodo cursus magna,
            vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus
            commodo.
          </p>
        </Col>
        <Col lg={{ size: 7, order: 1 }}>
          <PlaceholderImage height="500" width="500" />
        </Col>
      </>
    )}
  </Row>
);

export default FeatureRow;
