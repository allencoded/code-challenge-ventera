import React from "react";
import "./round-card.scss";
import PlaceholderImage from "../common/placeholder-image";
import { Button } from "reactstrap";

/**
 * Card which contains a round placeholder image, title, body, and a button
 */
const RoundCard = () => (
  <div className="round-card-wrapper">
    <PlaceholderImage width={140} height={140} />
    <h2>Heading</h2>
    <p className="round-card-body">
      Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam
      id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac
      consectetur ac, vestibulum at eros. Praesent commodo cursus magna.
    </p>
    <p>
      <Button>View Details</Button>
    </p>
  </div>
);

export default RoundCard;
