import React from "react";
import PropTypes from 'prop-types';

/**
 * Renders a placeholder image.
 * @param width
 * @param height
 * @returns {*}
 * @constructor
 */
const PlaceholderImage = ({ width, height }) => {
  const source = `https://placeholdit.imgix.net/~text?txtsize=33&txt=${width}%C3%97${height}&w=${width}&h=${height}`;
  return <img src={source} className="placeholder-image" alt="placeholder"/>;
};

PlaceholderImage.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
};

PlaceholderImage.defaultProps = {
  width: 100,
  height: 100,
};

export default PlaceholderImage;
