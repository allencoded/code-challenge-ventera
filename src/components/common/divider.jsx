import React from "react";
import './divider.scss';

/**
 * Divider is a horizontal rule that with some padding around it.
 */
const Divider = () => (
  <hr className="divider" />
);

export default Divider;
