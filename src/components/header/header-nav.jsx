import React from "react";
import {
  Button,
  Collapse,
  Form,
  Input,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";

export default class HeaderNav extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    };
  }

  toggle = () => this.setState({ isOpen: !this.state.isOpen });

  render() {
    return (
      <Navbar color="dark" dark expand="md">
        <NavbarBrand href="/">Carousel</NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="/">Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">Link</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">Disabled</NavLink>
            </NavItem>
          </Nav>
          <Form inline>
            <Input
              type="text"
              className="form-control mr-sm-2"
              placeholder="search"
              aria-label="Search"
            />
            <Button className={"my-2 my-sm-0"} outline color="success">
              Search
            </Button>
          </Form>
        </Collapse>
      </Navbar>
    );
  }
}
