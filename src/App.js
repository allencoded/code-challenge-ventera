import React, { Component } from 'react';
import './App.scss';
import HeaderNav  from './components/header/header-nav.jsx';
import HeaderCarousel from "./components/header/header-carousel";
import Page from './components/page';
import Footer from './components/footer';

class App extends Component {
  render() {
    return (
      <div className="app">
        <HeaderNav />
        <HeaderCarousel />
        <div className="page-wrapper">
          <Page />
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
